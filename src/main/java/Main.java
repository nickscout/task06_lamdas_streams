import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        task2();
    }

    public static List<Integer> generateList(int from, int to, int size) {
        List<Integer> list = new ArrayList<>(size);
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            list.add(random.ints(from, to).findFirst().getAsInt());
        }
        return list;
    }

    public static void task1() {
        List<Integer> list = generateList(0,100, 100);
        double average = list.stream().mapToInt(integer -> integer).average().getAsDouble();
        System.out.println(String.format("average : %f", average));
        System.out.println(String.format("min : %d", list.stream().mapToInt(integer -> integer).min().getAsInt()));
        System.out.println(String.format("max : %d", list.stream().mapToInt(integer -> integer).max().getAsInt()));
        System.out.println(String.format("sum : %d", list.stream().mapToInt(integer -> integer).sum()));
        System.out.println(String.format("threre are %d ints bigger than average", list.stream().mapToInt(integer -> integer).filter(i -> i > average).count()));
    }

    public static void task2() {
        Scanner sc = new Scanner(System.in);
        Set<String> strings = new HashSet<>();
        String str = "";
        while (true) {
            str = sc.nextLine();
            if (str.isEmpty()) {
                break;
            } else {
                strings.add(str);
            }
        }

        Set<String> uniqueStrings = strings.stream().distinct().collect(Collectors.toSet());
        System.out.println(String.format("# of unique words = %d", uniqueStrings.size()));
        StringBuilder sb = new StringBuilder("unique words in sorted order:");
        uniqueStrings.stream().forEachOrdered(word -> sb.append(word).append(", "));
        System.out.println(sb.toString());
        Map<String, Integer> wordOccurances = strings.stream()
                .collect(Collectors.toMap(key -> key, value -> (int) strings.stream().filter(s -> s.equals(value)).count()));
        System.out.println(String.format("word occurances: %s", wordOccurances.toString()));
        Map<String, Integer> wordLowerCaseOccurances = strings.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .filter(s -> s.equals(s.toLowerCase()))
                .collect(Collectors.toMap(k -> k, v -> (int) strings.stream().filter(s -> s.equals(v)).count()));
        System.out.println(String.format("lowercase chars occurances: %s", wordLowerCaseOccurances.toString()));
    }
}
